# htmlparser2-ultron
Extends htmlparser2 to add convenience methods and cover more use cases.

This package is a dependency of [Search Forms](https://pro-src.github.io/search-forms).

* Allows more intuitive use of the EventEmitter interface exposed by [htmlparser2](https://github.com/fb55/htmlparser2).
* Promotes [ultron](https://npmjs.com/package/ultron), used by [primus](https://github.com/primus/primus) for smart event management
* Adds convenience methods: `apply`, `chain`, `isolate`, `performRequest`, and `requests`
* Adds a property getter: `ultron` (Returns a new instance of ultron)
* Removes the need to pass callbacks as an argument to the constructor.
* Constructor now accepts a `request` option to supply default request options
* Allows passing `request` or `request-promise` functions in place of request options

### Events
The following events are now emitted on the parser instance so you don't have to pass event handlers to the constructor.
```js
const events = [
  "attribute",
  "cdatastart",
  "cdataend",
  "text",
  "processinginstruction",
  "comment",
  "commentend",
  "closetag",
  "opentag",
  "opentagname"
];
```

### Apply
This method is called whenever the parser's constructor is called or the parser is being chained to another parser.

```js
const { Parser } = require('htmlparser2-ultron');
const { MyParserA } = require('./parser-a');

class MyParserB extends Parser {
  constructor(options) {
    super(options)
  }
  apply(parser, options) {
    parser.doStuff = this.doStuff;
    
    const { ultron } = parser;
    ultron.on('opentag', (name, attribs) => {
      // ...
    });
  }
  doStuff() {
    console.log('stuff');
  }
}

const parser = new MyParserA();
parser.chain(MyParserB);
parser.doStuff(); // Logs stuff
```

### Chain
This method will allow you to mixin another parser by calling that parser's apply method.
Parsers may attach event listeners, add methods, or properties when being applied to another parser.

```js
const { Parser } = require('htmlparser2-ultron')

class MyParser extends Parser {
  constructor(options) {
    super(options)
  }
}

const parser = new Parser();
parser.chain(MyParser, { decodeEntities: true });
console.log(parser.options.decodeEntities); // Logs true
```

### Isolate
If you need to prevent other parsers from being notified of events during a single event loop then this method will come in handy.

```js
const { Parser } = requrie('htmlparser2-ultron');

class MyParser extends Parser {
  constructor(options) {
    super(options)
  }
  fetchAndParse(options) {
    this.peformRequest(options, (error, response, body) => {
      this.isolate(ultron => {
        // Only our parser will recieve events during the current event loop
        ultron.on('opentag', (name, attribs) => {
          // ...
        });
        
        this.write(body)
        // No need to remove the event listeners from this ultron instance!
      })
    });
  }
}
```

### PerformRequest
This method has almost the same function signature as mikeal's request and uses it under the hood except it has no return value.
If a location(URI) option was passed to the constructor then it will be used to resolve relative URI's.
If request options were passed to the constructor, those will be used as the request defaults.
If a request function was passed to the constructor, it will be used to make the request.

```js
const { Parser } = require('htmlparser2-ultron');

const parser = new Parser({ request: { headers }, location: 'https://example.com' });
```

### Requests
This method returns a promise that will not resolve until they are no active requests that have
been made with `performRequest` and each callback returns.
This includes requests made during the callback of `performRequest`.
Request errors are not emitted on the parser instance by default and will not cause promise rejection.

```js
const { Parser } = require('htmlparser2-ultron');
const request = require('request' || 'request-promise').defaults({ headers });

const parser = new Parser({ request, location: 'https://example.com' });
parser.requests().then(() => console.log('No active requests'));
```
